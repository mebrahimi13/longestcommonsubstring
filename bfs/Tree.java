package bfs;

import java.util.ArrayList;
import java.util.List;

public class Tree <T extends Object> {
	private List<Node<T>> nodes;
	private boolean isInitialState;
	
	public Tree()
	{
		this.nodes = new ArrayList<Node<T>>();
		this.isInitialState = true;
	}
	
	public void add(Node<T> node)
	{
		nodes.add(node);
	}
	
	public boolean bfsHasNext()
	{
		if( isInitialState )
		{
			isInitialState = false;
			return true;
		}
		
		boolean hasNext = false;
		for( int i = nodes.size() -1 ; i >= 0 ; i-- )
		{
			hasNext = nodes.get(i).circularHasNext();
			if( hasNext )
			{
				break;
			}
		}
		
		if( ! hasNext )
		{
			this.isInitialState = true;
		}
		return hasNext;
	}
	
	public List<T> bfsNext()
	{
		List<T> nextValues = new ArrayList<T>();
		for( Node<T> node : nodes )
		{
			nextValues.add(node.getNext());
		}
		return nextValues;
	}
	
	public static void main(String[] args)
	{
		Node<Integer> root = new Node<Integer>();
		root.add(new Integer[]{0, 1});
		
		Node<Integer> level1Node = new Node<Integer>();
		level1Node.add(new Integer[]{10, 11});
		
		Node<Integer> level2Node = new Node<Integer>();
		level2Node.add(new Integer[]{-1});
		
		Node<Integer> level3Node = new Node<Integer>();
		level3Node.add(new Integer[]{30, 31});
		
		Tree<Integer> tree = new Tree<Integer>();
		tree.add(root);
		tree.add(level1Node);
		tree.add(level2Node);
		tree.add(level3Node);
		
		while( tree.bfsHasNext() )
		{
			List<Integer> next = tree.bfsNext();
			System.out.println(next);
		}
		
		System.out.println("reset traverse: ");
		while( tree.bfsHasNext() )
		{
			List<Integer> next = tree.bfsNext();
			System.out.println(next);
		}
	}

}
