package bfs;

import java.util.ArrayList;
import java.util.List;

public class Node<T extends Object> {
	private List<T> values;
	private int traverseInd;
	
	public Node()
	{
		values = new ArrayList<T>();
		traverseInd = 0;
	}
	
	public void add(List<T> values)
	{
		this.values.addAll(values);
	}
	
	@SuppressWarnings("unchecked")
	public void add(T... values)
	{
		for( int i = 0 ; i < values.length ; i++ )
		{
			this.values.add(values[i]);
		}
	}
	
	public void add(T value)
	{
		this.values.add(value);
	}
	
	public boolean circularHasNext()
	{
		if( traverseInd + 1 == values.size() )
		{
			traverseInd = 0;
			return false;
		}
		else
		{
			traverseInd += 1;
			return true;
		}
	}

	public T getNext()
	{
		return values.get(traverseInd);
	}
}
