package sequence;

import java.util.ArrayList;
import java.util.List;

public class Sequence {
	private List<SequenceElements> sequence;
	
	public Sequence(final String sequenceStr)
	{
		this.sequence = new ArrayList<SequenceElements>();
		for( int i = 0 ; i < sequenceStr.length() ; i++ )
		{
			char elementChar = sequenceStr.charAt(i);
			SequenceElements element = SequenceElements.valueOf(String.valueOf(elementChar));
			sequence.add(element);
		}
	}
	
	public SequenceElements get(final int index)
	{
		return sequence.get(index);
	}
	
	public List<Integer> getOccurrences(final SequenceElements element)
	{
		List<Integer> result = new ArrayList<Integer>();
		for(int i = 0 ; i < sequence.size() ; i++ )
		{
			if( get(i) == element )
			{
				result.add(new Integer(i));
			}
		}
		return result;
	}
	
	public int length()
	{
		return sequence.size();
	}
}
