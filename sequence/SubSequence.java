package sequence;


public class SubSequence {
	private int begin;
	private int end;
	private boolean isBlank;
	
	public SubSequence(final int begin, final int end)
	{
		this.begin = begin;
		this.end = end;
		this.isBlank = false;
	}
	
	public boolean isBlank()
	{
		return isBlank;
	}
	
	public int getBegin()
	{
		return this.begin;
	}
	
	public int getEnd()
	{
		return this.end;
	}
	
	public void setBlank()
	{
		this.isBlank = true;
	}
	
	public int expandRight()
	{
		this.end += 1;
		return end;
	}
}
