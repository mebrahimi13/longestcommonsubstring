package main;

import java.util.ArrayList;
import java.util.List;

import bfs.Node;
import bfs.Tree;
import sequence.Sequence;
import sequence.SequenceElements;
import sequence.SubSequence;

public class ComputePath {
	private List<Sequence> sequences;
	
	public ComputePath(final List<Sequence> sequences)
	{
		this.sequences = sequences;
	}
	
	private Tree<Integer> createTraverseTree()
	{
		Tree<Integer> tree = new Tree<Integer>();
		for( SequenceElements element : SequenceElements.values() )
		{
			for( Sequence sequence : sequences )
			{
				List<Integer> occurrences = sequence.getOccurrences(element);
				if( occurrences.size() == 0 )
				{
					occurrences.add(-1);
				}
				
				Node<Integer> node = new Node<Integer>();
				node.add(occurrences);
				tree.add(node);
			}
		}
		return tree;
	}
	
	private List<SubSequence> createSubSequence(List<Integer> locations)
	{
		List<SubSequence> subSequences = new ArrayList<SubSequence>();
		for( Integer location : locations )
		{
			SubSequence subSequence = new SubSequence(location, location);
			if( location == -1 )
			{
				subSequence.setBlank();
			}
			subSequences.add(subSequence);
		}
		return subSequences;
	}
	
}
