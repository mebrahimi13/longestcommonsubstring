package main;

import java.util.ArrayList;
import java.util.List;

import sequence.Sequence;
import sequence.SequenceElements;
import sequence.SubSequence;

public class Path {
	private final List<Sequence> sequences;
	private List<SubSequence> path;
	
	public Path(final List<Sequence> sequences)
	{
		this.sequences = sequences;
		this.path = new ArrayList<SubSequence>();
	}
	
	public void add(final SubSequence subSequence)
	{
		this.path.add(subSequence);
	}
	
	public boolean isCommonPath(final double ratio)
	{
		int blankCount = 0;
		for( SubSequence subSequence : path )
		{
			if( subSequence.isBlank() )
			{
				blankCount += 1;
			}
		}
		return 1.0 * blankCount / path.size() >= ratio;
	}
	
	public boolean isExpandable()
	{
		int baseExpandIndex = path.get(0).getEnd() + 1;
		return baseExpandIndex < sequences.get(0).length();
		
	}
	
	public void expandRight()
	{
		int baseExpandIndex = path.get(0).getEnd() + 1;
		SequenceElements baseExpandChar = sequences.get(0).get(baseExpandIndex);

		int sequenceIndex = 0;
		
		for(SubSequence subSequence : path)
		{
			int expandIndex = subSequence.expandRight();
			if( expandIndex == sequences.get(sequenceIndex).length() )
			{
				subSequence.setBlank();
			}
			
			SequenceElements expandChar = sequences.get(sequenceIndex).get(expandIndex);
			
			if( expandChar != baseExpandChar )
			{
				subSequence.setBlank();
			}
			
			sequenceIndex += 1;
		}
	}
}
